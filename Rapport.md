
# TP3

## Commandes utilisées

### Partie 1

Dans le dossier tp-rebase-2020, on ajoute une nouvelle remote vers le fork que l'on vient de faire :

    git remote add personal git@gitlab.com:theo.welsch/tp-rebase-2020.git

### Partie 2
   
   Après avoir créé l'issue, on créé une nouvelle branche avec le numéro de l'issue:
   

    git checkout -b 1-customize-readme

On push la branche sur la remote personal (apres avoir modifié le fichier readme l'avoir add et l'avoir commit)

    git push personal

### Partie 3

Une fois toute la partie sur gitlab pour faire le merge request, on va rappatrier les changements en local

    git checkout master; git pull personal



